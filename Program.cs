﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1_Koloda
{
    class Program
    {
        static void Main(string[] args)
        {
            //3. zadatak
            //kreiranje objekata
            Notes note1, note2, note3;

            //1. objekt
            note1 = new Notes();
            Console.WriteLine("Enter content of note: ");
            string tempContent = Console.ReadLine();
            note1.Content = tempContent;
            //Console.WriteLine("Author: " + note1.Author + " \nContent: " + note1.Content + "\n");
            Console.WriteLine(note1.ToString());

            //2. objekt
            Console.WriteLine("Enter a author: ");
            string tempAuthor = Console.ReadLine();
            note2 = new Notes(tempAuthor);
            //Console.WriteLine("Author: " + note2.Author + " \nContent:" + note2.Content + "\n");
            Console.WriteLine(note2.ToString());

            //3. objekt
            Console.WriteLine("Enter a author: ");
            tempAuthor = Console.ReadLine();
            Console.WriteLine("Enter a priority: ");
            string tempPriority = Console.ReadLine();
            note3 = new Notes(tempAuthor, Int32.Parse(tempPriority));
            //Console.WriteLine("Author: " + note3.Author + " \nContent:" + note3.Content + "\n");
            Console.WriteLine(note3.ToString());
            Console.WriteLine("\n");

            NotesWithTime notetime1, notetime2, notetime3;
            notetime1 = new NotesWithTime();
            Console.WriteLine("Enter content of note: ");
            tempContent = Console.ReadLine();
            notetime1.Content = tempContent;
            Console.WriteLine(notetime1.ToString());

            notetime2 = new NotesWithTime();
            Console.WriteLine("Enter content of note: ");
            tempContent = Console.ReadLine();
            notetime1.Content = tempContent;
            Console.WriteLine(notetime1.ToString());

            notetime3 = new NotesWithTime();
            Console.WriteLine("Enter content of note: ");
            tempContent = Console.ReadLine();
            notetime1.Content = tempContent;
            Console.WriteLine(notetime1.ToString());

            //7. zadatak
            ToDoList sampleToDoList;
            sampleToDoList = new ToDoList();
            sampleToDoList.AddNote(notetime1);
            sampleToDoList.AddNote(notetime2);
            sampleToDoList.AddNote(notetime3);

            sampleToDoList.RemoveHighestPriority();

            Console.WriteLine(sampleToDoList.ToString());

        }
    }
}
