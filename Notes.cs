﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1_Koloda
{
    class Notes
    {
        private string author;
        private string content;
        private int priority; //0 - high, 1- medium, 2 - low

        //konstruktor
        public Notes()
        {
            this.author = "Anon";
            this.content = "Lorem ipsum dolor sit";
            this.priority = 3;
        }

        public Notes(string rAuthor)
        {
            this.author = rAuthor;
            this.content = "Lorem ipsum dolor sit";
            this.priority = 3;
        }

        public Notes(int rPriority)
        {
            this.author = "Anon";
            this.content = "Lorem ipsum dolor sit";
            this.priority = rPriority;
        }

        public Notes(string rAuthor, int rPriority)
        {
            this.author = rAuthor;
            this.content = "Lorem ipsum dolor sit";
            this.priority = rPriority;
        }


        //return class atributes
        /* --zamjena za svojstva--
         * 
        public string getAuthor() { return this.author; }
        public string getContent() { return this.content; }
        public int getPriority() { return this.priority; }

        //set class atributes
        public void setConent(string rContent) { this.content = rContent; }
        public void getPriority(int rPriority) { 
            if(rPriority<=0)
            {
                priority = 0;
            }
            else if(rPriority>=2)
            {
                priority = 2;
            }
            else
            {
                priority = rPriority;
            } 
 }

        */

        //4. zadatak
        public string Author
        {
            get { return this.author; }
            //nema SET, isključivo u konstruktoru - nema pormjene kasnije
        }

        public string Content
        {
            get { return this.content; }
            set { this.content = value; }
        }

        public int Priority
        {
            get { return this.priority; }
            set {
                if (value <= 0)
                {
                    this.priority = 0;
                }
                else if (value >= 2)
                {
                    this.priority = 2;
                }
                else
                {
                    this.priority = value;
                }
            }
        }

        //5. zadatak
        public override string ToString()
        {
            return "Author: " + Author + "\nContent: " + Content + "\nPriority: " + Priority + "\n";
        }
    }
}
