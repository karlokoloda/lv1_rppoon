﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1_Koloda
{
    class ToDoList
    {
        private List<NotesWithTime> notes;

        public ToDoList() { notes = new List<NotesWithTime>(); }
        public void AddNote(NotesWithTime newNote) { notes.Add(newNote); }
        public int getHighPriority()
        {
            int highestPriority = notes[0].Priority;
            foreach (NotesWithTime note in notes)
            {
                if (highestPriority > note.Priority) {
                    highestPriority = note.Priority;
                }
            }
            return highestPriority;
        }

        public void RemoveHighestPriority()
        {
            int highestPriority = getHighPriority();
            foreach (NotesWithTime note in notes.ToList())
            {
                if (highestPriority ==¸note.Priority) {
                    notes.Remove(note);
                }
            }
        }

        public void ClearNotes()
        {
            notes.Clear();
        }

        public NotesWithTime getSelectedNote(int i)
        {
            return notes[i];
        }

        public override string ToString()
        {
            StringBuilder listPrintFormat = new StringBuilder("\n");
            foreach (NotesWithTime note in notes)
            {
                listPrintFormat.Append(note + "\n");
            }
            return listPrintFormat.ToString();
        }


    }
}
