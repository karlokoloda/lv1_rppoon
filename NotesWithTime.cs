﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1_Koloda
{
    class NotesWithTime : Notes
    {
        DateTime timestamp;
        //6. zadatak

        //konstruktori
        public NotesWithTime()
        {
            timestamp = DateTime.Now;
        }

        public NotesWithTime(string author) : base(author)
        {
            this.timestamp = DateTime.Now;
        }

        public NotesWithTime(int priority) : base(priority)
        {
            this.timestamp = DateTime.Now;
        }

        public NotesWithTime(string author, int priority) : base(author, priority)
        {
            this.timestamp = DateTime.Now;
        }

        //get-set klase
        public DateTime Timestamp
        {
            get { return this.timestamp; }
            set { this.timestamp = value; }
        }

        //override ToString() funkcije
        public override string ToString()
        {
            return "Author: " + Author + "\nContent: " + Content + "\nPriority: " + Priority + "\nTime Created: " + Timestamp + "\n";
        }
    }
}
